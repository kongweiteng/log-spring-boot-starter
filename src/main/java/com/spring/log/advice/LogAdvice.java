package com.spring.log.advice;


import com.spring.log.config.LogProperties;
import com.spring.log.entity.LogInfo;
import com.spring.log.service.ILogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * 统一日志处理
 */
@Aspect
@Slf4j
public class LogAdvice implements Monitor{


    private Boolean monitorStatus ;

    @Autowired
    private ILogService ILogService;

    @Autowired
    private LogProperties logProperties;

    @PostConstruct
    public void init() {
        //获取开关信息
        Boolean context = logProperties.getContext();
        log.info("---日志切面 初始化--- 全局开关："+context);
        this.monitorStatus=context;
    }

    @Pointcut("execution(* com..*..*Controller.*(..))")
    public void controllerMethodPointcut() {
    }

    @Around(value = "@annotation(com.spring.log.annotation.LogAnnotation)")
    public Object logSave(ProceedingJoinPoint joinPoint) throws Throwable {
        long l = System.currentTimeMillis();
        LogInfo logInfo = new LogInfo();
        System.err.println("方法执行前 ----------------" + Thread.currentThread().getName());
        System.err.println("执行编号："+l);
        ILogService.save();
        try {
            Object object = joinPoint.proceed();
            System.err.println("方法执行后 ++++++++++++++响应时间为: " + (System.currentTimeMillis() - l));
            System.err.println("执行编号："+l);

            return object;
        } catch (Exception e) {
            throw e;
        } finally {
        }

    }

    @Around(value = "controllerMethodPointcut()")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        //获取开关信息
        if(monitorStatus){
            Date date1= new Date();
            Object result = null;
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();

            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            ServletRequestAttributes sra = (ServletRequestAttributes) ra;
            HttpServletRequest request = sra.getRequest();
            //请求编号记忆
            long count=System.currentTimeMillis();
            /* 打印请求地址及参数 */
            log.info("请求开始，请求地址："+request.getRequestURL()+",请求编号"+count);
            //类名  //方法名
            Class<?> aClass = joinPoint.getTarget().getClass();
            String name = joinPoint.getSignature().getName();
            log.info("类名称："+aClass+" ,方法名称："+name);

            StringBuffer sb = new StringBuffer() ;
            InputStream is = request.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String s = "" ;
            while((s=br.readLine())!=null){
                sb.append(s) ;
            }
            log.info("请求参数："+sb.toString());
            result = joinPoint.proceed();
            Date date2= new Date();
            log.info("请求结束，本次请求时间："+(date2.getTime()-date1.getTime())+",请求编号"+count);
            return result;
        }else {
            return  joinPoint.proceed();
        }

    }

    public void setMonitorActive(boolean active) {
        this.monitorStatus=active;
    }
}
