package com.spring.log.advice;

public interface Monitor {
    void setMonitorActive(boolean active);
}
