package com.spring.log.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author Admin
 */
@Configuration
@Slf4j
public class TaskExecutorConfig {

    @Bean
    public TaskExecutor taskExecutor() {
        int poolSize = Runtime.getRuntime().availableProcessors() * 2;
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //配置核心线程数
        executor.setCorePoolSize(poolSize);
        //配置最大线程数
        executor.setMaxPoolSize(poolSize);
        //配置队列大小  合适的大小 防止资源耗尽
        executor.setQueueCapacity(200);
        //配置线程池中的线程的名称前缀
        executor.setThreadNamePrefix("log-thread");
        /**
         * 所谓的拒绝策略就是 在线程池中的线程都有任务时，有新的异步任务时的处理 策略
         */
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }

}
