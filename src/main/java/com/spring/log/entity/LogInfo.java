package com.spring.log.entity;

import lombok.Data;

/**
 * @author Admin
 */
@Data
public class LogInfo {

    private long responseTime;

    private String remark;
}
